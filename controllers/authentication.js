const User = require('../models/user');
const jwt = require('jwt-simple');
const config = require('../config');

const axios = require('axios');

const apiTaskManagerPrefix = config.taskManagerApi;

function tokenForUser(user){
    //jwt has a subject as sub property that define who is this token about
    // who this token belongs to
    const timestamp = new Date().getTime();
    return jwt.encode({ sub: user.id, iat: timestamp },config.secret);
}

exports.signup = function(req, res, next){
    
    // --0
    const email = req.body.email;
    const password = req.body.password;

    if(!email || !password){
        return res.status(422).send({
            error: "You must provide email and password!"
        });
    }
    // --1
    User.findOne({ email: email }, function(err, existingUser){
        if( err ){ return next(err); }

        if( existingUser ){
            // 422 - Unprocessable entity
            return res.status(422).send({ error: "Email is in use." });
         }

         const user = new User({
             email: email,
             password: password
         });
         user.save(function(err){
             if(err) {
                 return next(err);
            }                       
        });
        /// send request to insert net token to server
        const userId = user._id;
        const token = tokenForUser(user);
        
        axios.post(`${apiTaskManagerPrefix}/api/guard/tokens`, {id: userId, token: token})
        .then(response => {
            console.log('----------------------------');
            console.log('THEN');
            console.log(response.data);
            
            res.json({ token: tokenForUser(user) });
        })
        .catch(response => {
            console.log('----------------------------');        
            console.log('ERROR:');
            console.log(response.data);
        });
    });
}

exports.signin = function(req, res, next){
    // already has em and pwd
    //just to need give them token
    const userId = req.user._id;
    const token = tokenForUser(req.user);

    axios.post(`${apiTaskManagerPrefix}/api/guard/tokens`, {id: req.user._id, token: token})
    .then(response => {
        res.send({ token: token });
    })
    .catch(response => {
        console.log('----------------------------');        
        console.log('ERROR:');
        console.log(response.data);
    });
}
